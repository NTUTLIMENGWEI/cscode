function [ theta ] = CS_CoSaMP(y,A,K)
    [y_rows,y_columns] = size(y);
    if y_rows < y_columns
       y = y';                              % y為列向量
    end
    [M,N] = size(A);                        % 感測矩陣A為M*N矩陣
    theta = zeros(N,1);                     % 用於存放恢復的theta(行向量)
    Pos_theta = [];                         % 用於疊代過程中存放A被選擇的行號
    r_n = y;                                % 初始化的殘差(residual)為y
    for iter=1:K                            % 疊代次數為K次

        %(1) Identification
        product = A'*r_n;                   % 感知矩陣A各行與殘差的內積
        [val,pos]=sort(abs(product),'descend');
        Js = pos(1:2*K);                    % 挑選出內積值最大的2K行

        %(2) Support Merger
        Is = union(Pos_theta,Js);           % Pos_theta與Js聯集

        %(3) Estimation
        if length(Is) <= M                  % At的列數要大於行數，為最小平方法的基礎(行必須彼此線性獨立)
            At = A(:,Is);                   % 將A的這幾行組成矩陣At
        else                                % At的行數大於列數，列必為線性相依的,At'*At將不可逆
            if iter == 1
               theta_LS = 0;
            end
            break;                          % 跳出for循環
        end
        %y=At*theta，以下求theta使用最小平方法(Least Squares Method)
        theta_LS = (At'*At)^(-1)*At'*y;     % 最小平方法

        %(4) Pruning
        [val,pos]=sort(abs(theta_LS),'descend');

        %(5) Sample Update
        Pos_theta = Is(pos(1:K));
        theta_LS = theta_LS(pos(1:K));
        % 更新殘差r_n
        r_n = y - At(:,pos(1:K))*theta_LS;  % At(:,pos(1:K))*theta_ls是y在At(:,pos(1:K))行空間上的正交投影
        if norm(r_n)<1e-1                   % 重複步驟至殘差r_n=0
           break;                           % 跳出for循環
        end
    end
    theta(Pos_theta)=theta_LS;              % 重建出theta
end