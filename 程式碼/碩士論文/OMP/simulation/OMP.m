% set(0,'defaultfigurecolor','w')    % 白底
% clear all;close all;clc;
%% Matlab模擬訊號
F0=60;                            
SN=32;                                     
Fs=SN*60;                         
time=1;
t1=0 : 1/Fs :(time*Fs-1)/Fs;    
%% 輸入 Matlab模擬訊號
%----------------Matlab模擬訊號(依序為0%,5%,8%,11%,13%,15%)----------------
c = 1*cos(2*pi*F0*t1+pi/6);
% c = 1*cos(2*pi*F0*t1+pi/6)+0.05*cos(3*2*pi*F0*t1+3*(pi/6)); 
% c = 1*cos(2*pi*F0*t1+pi/6)+0.05*cos(3*2*pi*F0*t1+3*(pi/6))+0.045*cos(5*2*pi*F0*t1+5*(pi/6))+0.0433*cos(7*2*pi*F0*t1+7*(pi/6));
% c = 1*cos(2*pi*F0*t1+pi/6)+0.07*cos(3*2*pi*F0*t1+3*(pi/6))+0.045*cos(5*2*pi*F0*t1+5*(pi/6))+0.0435*cos(7*2*pi*F0*t1+7*(pi/6))+0.041*cos(9*2*pi*F0*t1+9*(pi/6))+0.04*cos(11*2*pi*F0*t1+11*(pi/6));
% c = 1*cos(2*pi*F0*t1+pi/6)+0.09*cos(3*2*pi*F0*t1+3*(pi/6))+0.06*cos(5*2*pi*F0*t1+5*(pi/6))+0.045*cos(7*2*pi*F0*t1+7*(pi/6))+0.038*cos(9*2*pi*F0*t1+9*(pi/6))+0.034*cos(11*2*pi*F0*t1+11*(pi/6))+0.02*cos(13*2*pi*F0*t1+13*(pi/6))+0.0132*cos(15*2*pi*F0*t1+15*(pi/6));
% c = 1*cos(2*pi*F0*t1+pi/6)+0.09*cos(3*2*pi*F0*t1+3*(pi/6))+0.07*cos(5*2*pi*F0*t1+5*(pi/6))+0.064*cos(7*2*pi*F0*t1+7*(pi/6))+0.053*cos(9*2*pi*F0*t1+9*(pi/6))+0.038*cos(11*2*pi*F0*t1+11*(pi/6))+0.03*cos(13*2*pi*F0*t1+13*(pi/6))+0.0158*cos(15*2*pi*F0*t1+15*(pi/6));
%% OMP參數
CR = 0.5;                            % 壓縮率 (Compression Rate)
IS = c;                              % 輸入訊號
SL = 1920;                           % 選取長度(Select Length)
ft=fft(IS(1:SL));
threshold=12;
K = length(find((abs(ft))>threshold));
N = length(IS(1:SL));              
M = double(int32(N*CR));
Phi = randn(M,N)/sqrt(M);            % 測量矩陣為均值0、方差1/M的隨機高斯矩陣
Psi = dftmtx(N)/sqrt(N);             % 稀疏矩陣為離散傅立葉變換基 另種打法：fft(eye(N))/sqrt(N)
A = Phi * Psi;
y = Phi * IS(1:SL)';
%% OMP還原演算法
tic
theta = CS_OMP(y,A,K);               % 重建訊號theta
IS_new = (Psi * theta)';             % x = Psi*theta
time=toc;
IS_new = real(IS_new);
% fprintf('over')
%% 保存0.05秒OX圖及絕對誤差圖
figure_time=0.05;
figure_t=0 : 1/Fs :(figure_time*Fs-1)/Fs;    

figure(1)
box on
hold on
plot(figure_t,IS(1:size(IS,2)*0.05),'-x')
plot(figure_t,IS_new(1:size(IS_new,2)*0.05),'-o')
hold off
xlabel('Time(s)');                      
ylabel('Ampitude');                    
title('Time Domain Signal');
legend('Original Signal','Restore Signal')
% axis([0,0.05,-1.31,1.31])

figure(2)
abs_error = abs(IS-IS_new);
plot(figure_t,abs_error(1:size(abs_error,2)*0.05))
xlabel('Time(s)');                      
ylabel('Ampitude'); 
title('Errror');
% axis([0,0.05,0,inf])
%% 誤差百分比
error_percentage = (norm(IS-IS_new)/norm(IS))*100;
fprintf('error_percentage   %7.4f\n',error_percentage);
fprintf('time               %7.4f\n',time);