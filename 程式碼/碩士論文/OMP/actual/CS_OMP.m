function [theta] = CS_OMP(y,A,K)  
    [y_rows,y_columns] = size(y);  
    if y_rows<y_columns  
        y = y';                           % y為列向量
    end  
    [M,N] = size(A);                      % 感測矩陣A為M*N矩陣  
    theta = zeros(N,1);                   % 用於存放恢復的theta(行向量)
    At = zeros(M,K);                      % 用於疊代過程中存放A被選擇的行 
    Pos_theta = zeros(1,K);               % 用於疊代過程中存放A被選擇的行號  
    r_n = y;                              % 初始殘差為y(Inital residue) 
    for ii=1:K                            % 疊代次數為K次
        product = A'*r_n;                 % 感知矩陣A各行與殘差的內積  
        [val,pos] = max(abs(product));    % 找出與殘差內積最大值(相關性最大的行)  
        At(:,ii) = A(:,pos);              % 儲存這一行  
        Pos_theta(ii) = pos;              % 儲存這一行的序號  
        A(:,pos) = zeros(M,1);            % 清空A的這一行
        %y=At(:,1:ii)*theta，求theta的最小平方法(LS)  
        theta_LS = (At(:,1:ii)'*At(:,1:ii))^(-1)*At(:,1:ii)'*y;% 最小平方法(least square)
        %At(:,1:ii)*theta_ls是y在At(:,1:ii)行空間上的正交投影  
        r_n = y-At(:,1:ii)*theta_LS;      % 更新殘差          
    end  
    theta(Pos_theta)=theta_LS;            % 重建出theta  
end