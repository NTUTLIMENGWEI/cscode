clear all;close all;clc;
% clearvars -except sample1 sample2 sample3 sample4 sample5 sample6 sample7 sample8 sample9 sample10 sample11 sample12 sample13 sample14 sample15 sample16 sample17 sample18
%% 輸入功率標準源諧波訊號
%--------------功率標準源諧波訊號(依序為0%,5%,8%,11%,13%,15%)--------------
c = sample1;
% c = sample2;
% c = sample3;
% c = sample4;
% c = sample5;
% c = sample6;
%% OMP所需參數
CR = 0.5;                            % 壓縮率 (Compression Rate)
IS = c;                              % 輸入訊號
SL = 1920;                           % 選取長度(Select Length)
ft=fft(IS(1:SL));
threshold=100;
K = length(find((abs(ft))>threshold));
N = length(IS(1:SL));              
M = double(int32(N*CR));
Phi = randn(M,N)/sqrt(M);            % 測量矩陣為均值0、方差1/M的隨機高斯矩陣
Psi = dftmtx(N)/sqrt(N);             % 稀疏矩陣為離散傅立葉變換基 另種打法：fft(eye(N))/sqrt(N)
A = Phi * Psi;
y = Phi * IS(1:SL)';
%% OMP還原演算法
tic
theta = CS_OMP(y,A,K);               % 恢復重建訊號theta
IS_new = (Psi * theta)';             % x = Psi*theta
time=toc;
IS_new = real(IS_new);
% fprintf('over')
%% 保存0.05秒OX圖及絕對誤差圖
figure_time=0.05;
figure_t=0 : 1/Fs :(figure_time*Fs-1)/Fs;    

figure(1)
box on
hold on
plot(figure_t,IS(1:size(IS,2)*0.05),'-x')
plot(figure_t,IS_new(1:size(IS_new,2)*0.05),'-o')
hold off
xlabel('Time(s)');                      
ylabel('Ampitude');                    
title('Time Domain Signal');
legend('Original Signal','Restore Signal')
axis([0,0.05,-200,200])
% axis([0,0.05,-160,160])
figure(2)
abs_error = abs(IS-IS_new);
plot(figure_t,abs_error(1:size(abs_error,2)*0.05))
xlabel('Time(s)');                      
ylabel('Ampitude'); 
title('Errror');
% axis([0,0.05,0,inf])
%% 誤差百分比
error_percentage = (norm(IS-IS_new)/norm(IS))*100;
fprintf('error_percentage   %7.4f\n',error_percentage);
fprintf('time               %7.4f\n',time);