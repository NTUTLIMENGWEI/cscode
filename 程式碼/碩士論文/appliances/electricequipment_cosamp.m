close all;
clearvars -except oven_thd ricecooker_thd microwave_thd oven ricecooker microwave

Fs=1920;
% c=oven((1:Fs),2)*100;          %烤箱
% c=ricecooker((1:Fs),2)*100;   %電鍋
c=microwave((1:Fs),2)*100;    %微波爐
%% CoSamp
CR = 0.5;                            % 壓縮率 (Compression Rate)
IS = c';                             % 輸入訊號
SL = 1920;                           % 選取長度(Select Length)
ft=fft(IS(1:SL));
threshold=12;
K = length(find((abs(ft))>threshold));
N = length(IS(1:SL));              
M = double(int32(N*CR));
Phi = randn(M,N)/sqrt(M);            % 測量矩陣為均值0、方差1/M的隨機高斯矩陣
Psi = dftmtx(N)/sqrt(N);             % 稀疏矩陣為離散傅立葉變換基 另種打法：fft(eye(N))/sqrt(N)
A = Phi * Psi;
y = Phi * IS(1:SL)';

tic
theta = CS_CoSaMP(y,A,K);         % 恢復重建訊號theta
IS_new = (Psi * theta)';             % x = Psi*theta
time=toc;
IS_new = real(IS_new);

%% 保存0.05秒OX圖及絕對誤差圖
figure_time=1;
figure_t=0 : 1/Fs :(figure_time*Fs-1)/Fs;    
% 
figure(1)
box on
hold on
plot(figure_t,IS(1:size(IS,2)*figure_time),'-x')
plot(figure_t,IS_new(1:size(IS_new,2)*figure_time),'-o')
hold off
xlabel('Time (s)');                      
ylabel('Current (A)');                    
title('Microwave Oven Time Domain Signal');
legend('Original Signal','Restore Signal')
axis([0,0.05,-20,20])

figure(2)
abs_error = abs(IS-IS_new);
plot(figure_t,abs_error(1:size(abs_error,2)*figure_time))
xlabel('Time(s)');                      
ylabel('Current (A)');                    
title('Errror');
axis([0,0.05,-inf,inf])
%% 誤差百分比
error_percentage = (norm(IS-IS_new)/norm(IS))*100;
fprintf('error_percentage   %7.4f\n',error_percentage);
fprintf('time               %7.4f\n',time);
%% 原始訊號THD-----------------------------------------------------------------------------
% testdata=c;
% % THD逐點重建 (2000points)
% i=1;
% for j=1:length(testdata)-1919
%     sort_testdata(i,:)=testdata(j:j+1919);
%     i=i+1
% end
% for it=1:size(sort_testdata,1) 
% THD_VALUE(it,:)=10^(0.05*(thd(sort_testdata(it,:),1920,49)))*100;
% end
% figure(1)
% plot(THD_VALUE)
% %--------------------------------------------------------------------------
% %% OMP計算兩秒
% CR = 0.5;   
% j=1
% for i=1:Fs:Fs*2
% IS = c(i:i+1919);
% fft_result=abs(fft(IS));
% K = length(find(fft_result>12));
% % K=200;
% N = length(IS);   
% M = double(int32(N*CR));
% Psi = dftmtx(N)/sqrt(N);
% %% OMP
% Phi0 = randn(M,N)/sqrt(M);
% A = Phi0 * Psi;
% y = Phi0 * IS;
% theta0 = CS_OMP(y,A,K);
% IS_new0 = (Psi * theta0)';  
% IS_new0 = real(IS_new0);
% save(j,:)=IS_new0(1,:);
% j=j+1;
% end
% save_omp=[save(1,1:1920) save(2,1:1920)];
% testdata1=save_omp;
% % % THD逐點重建 (2000points)
% i=1;
% for j=1:length(testdata1)-1919
%     sort_testdata1(i,:)=testdata1(j:j+1919);
%     i=i+1
% end
% for c=1:size(sort_testdata1,1) 
% THD_VALUE1(c,:)=10^(0.05*(thd(sort_testdata1(c,:),1920,49)))*100;
% end
% figure(2)
% plot(THD_VALUE1)
% figure(3)
% error=abs(THD_VALUE-THD_VALUE1)
% plot(error)