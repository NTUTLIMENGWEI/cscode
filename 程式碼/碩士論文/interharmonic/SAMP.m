% set(0,'defaultfigurecolor','w')
clear all;close all;clc;
%% Matlab模擬訊號
F0=60;                            
SN=32;                                     
Fs=SN*60;                         
time=1;
t1=0 : 1/Fs :(time*Fs-1)/Fs;    
%% 輸入 Matlab模擬訊號 or 功率標準源諧波訊號
%------------------------------Matlab模擬訊號(間諧波)------------------------------
c = 1*cos(2*pi*F0*t1+pi/6)+0.05*cos(2*pi*3.05*F0*t1+pi/6);
%% SAMP所需參數
CR = 0.5;                            % 壓縮率 (Compression Rate)
IS = c;                              % 輸入訊號
SL = Fs;                             % 選取長度(Select Length)
N = length(IS(1:SL));    
M = double(int32(N*CR));
Phi = randn(M,N)/sqrt(M);            % 測量矩陣為均值0、方差1/M的隨機高斯矩陣
Psi = dftmtx(N)/sqrt(N);             % 稀疏矩陣為離散傅立葉變換基 另種打法：fft(eye(N))/sqrt(N)
A = Phi * Psi;
y = Phi * IS(1:SL)';
%% SAMP還原演算法
tic
theta = CS_SAMP( y,A,1 );
IS_new = (Psi * theta)';
time=toc;
IS_new = real(IS_new);
% fprintf('over')
%% 保存0.05秒OX圖及絕對誤差圖
figure_time=0.05;
figure_t=0 : 1/Fs :(figure_time*Fs-1)/Fs;    

figure(1)
box on
hold on
hold on
plot(figure_t,IS(1:size(IS,2)*0.05),'-x')
plot(figure_t,IS_new(1:size(IS_new,2)*0.05),'-o')
hold off
xlabel('Time(s)');                      
ylabel('Ampitude');                    
title('Time Domain Signal');
legend('Original Signal','Restore Signal')
% axis([0,0.05,-1.31,1.31])

figure(2)
abs_error = abs(IS-IS_new);
plot(figure_t,abs_error(1:size(abs_error,2)*0.05))
xlabel('Time(s)');                      
ylabel('Ampitude'); 
title('Errror');
% axis([0,0.05,0,inf])
%% 誤差百分比
error_percentage = (norm(IS-IS_new)/norm(IS))*100
fprintf('error_percentage   %7.4f\n',error_percentage);
fprintf('time               %7.4f\n',time);
%% 一秒的作圖
% figure(3)
% subplot(2,1,1)
% hold on
% plot(t1,IS)
% plot(t1,IS_new)
% xlabel('Time(s)');                      
% ylabel('Ampitude');                    
% title('Time Domain Signal');
% legend('Original Signal','Restore Signal')
% hold off
% box on
% 
% subplot(2,1,2)
% hold on
% abs_error = abs(IS-IS_new);
% plot(t1,abs_error)
% xlabel('Time(s)');                      
% ylabel('Ampitude'); 
% title('Errror');
% hold off
% box on