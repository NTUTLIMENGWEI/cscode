function [ theta ] = CS_SAMP( y,A,S )
    [y_rows,y_columns] = size(y);
    if y_rows < y_columns
        y = y';                    % y為列向量
    end
    [M,N] = size(A);               % 感測矩陣A為M*N矩陣
    theta = zeros(N,1);            % 用來儲存恢復的theta(行向量：Nx1)
    Pos_theta = [];                % 迭代過程中儲存感知矩陣A被選擇的行向量 (Empty finalist)
    r_n = y;                       % 初始殘差為y(Inital residue)
    L = S;                         % 初始步階長(Size of the finalist in the first stage)
    Stage = 1;                     % 初始Stage(Stage index)
    IterMax = M;                   % 最多迭代次數
    for ii=1:IterMax               % (Iteration index)
% (1)Preliminary Test(初步測試)
        product = A'*r_n;          % 感知矩陣A各行與殘差內積
        [val,pos]=sort(abs(product),'descend'); % 降冪排列
        Sk = pos(1:L);             % 挑出最大的L個      
% (2)Make Candidate List(製作候選清單)
        Ck = union(Pos_theta,Sk); 
% (3)Final Test(最終測試)
        if length(Ck)<=M
            At = A(:,Ck);          % 將A的這幾個行向量成矩陣At，又稱Ck版感知矩陣
        else
            theta_LS=0;
            break;
        end
        % y=At*theta，以下求theta的最小平方法(LS)：以下以theta_LS代替
        theta_LS = (At'*At)^(-1)*At'*y;% 最小平方法
        [val,pos]=sort(abs(theta_LS),'descend');% 降冪排列
        F = Ck(pos(1:L));              % 挑出最大的L個放入F      
% (4)Compute Residue(計算殘差)
        % A(:,F)*theta_LS是y在A(:,F)行空間上的正交投影
        theta_LS = (A(:,F)'*A(:,F))^(-1)*A(:,F)'*y;
        r_new = y - A(:,F)*theta_LS;   % 更新殘差r
        if norm(r_new)<1e-1            % halting condition true 
            Pos_theta = F;
            % r_n = r_new;             % 更新r_n以方便輸出最新的r_n
            break;                     % 跳出循環
        elseif norm(r_new)>=norm(r_n)  % stage switching
            Stage = Stage + 1;         % Up位置date the stage index
            L = Stage*S;               % Update the size of finalist
            if ii == IterMax           % 最後一次循環
                Pos_theta = F;         % 未防止出錯，更新Pos_theta用以與theta_LS匹配，防止出錯
            end
            % ii = ii - 1;             % 迭代次數不更新
        else
            Pos_theta = F;             % Update the finalist Fk
            r_n = r_new;               % Update the residue rk
        end
    end
    theta(Pos_theta)=theta_LS;         % 恢复出theta
end