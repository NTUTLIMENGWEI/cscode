function [avg_data]=avg(length,avg_size,fresult)

for i = 1:length;
    tmp = 0.0;
    if(i<avg_size)
        for j = 1 : i 
            tmp = tmp + fresult(i - (j-1));
        end;
        avg_data(i) = tmp / i;
    else
        for j = 1 : avg_size
            tmp = tmp + fresult(i - (j-1)) ;
        end;
        avg_data(i) = tmp / avg_size;
    end;    
end;