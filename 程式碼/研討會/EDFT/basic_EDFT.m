clear all;close all;clc;

%waveform basic setting.
V=1;
f0=59.123456;
N=16;
fs=59.1234*N;
Ts=0:1/(fs):(fs-1)/fs;
phi=0;

%波形模擬
x=V*cos(2*pi*f0*Ts)+0.05*cos(2*pi*3*f0*Ts)+0.04*cos(2*pi*5*f0*Ts)+0.03*cos(2*pi*7*f0*Ts);     %待測訊號
% x=V*cos(2*pi*f0*Ts);     
% x=gg/110+0.05*cos(2*pi*3*f0*Ts)+0.04*cos(2*pi*5*f0*Ts)+0.03*cos(2*pi*7*f0*Ts)
% x=V*cos(2*pi*f0*Ts)+0.05*cos(2*pi*3*f0*Ts)+0.04*cos(2*pi*5*f0*Ts)+0.03*cos(2*pi*7*f0*Ts)    

subplot(3,1,1);
plot(Ts,x);             %畫出輸入波形
title('輸入波形訊號');   %標題為"輸入波形訊號"
xlabel('time(sec)');    %x軸為時間(s)
ylabel('Ampitude');     %y軸為振幅

tic

length = numel(x)-16;
plotmap = 1:length-2;
coslength = length;

for i=1:coslength
    for point = 1:16 
        y(point,i) = x(i+(point - 1));
    end;
end;

tmp = fft(y,16);

for i=1:length
    fresult(i) = tmp(2,i);
end

length = numel(fresult);

[amp_x,ang_x,realfreq_x] = EDFT(length,fresult);
amp_x_len = numel(amp_x);
avg_amp_x = avg(amp_x_len ,128 ,amp_x);
avg_freq_x = avg(amp_x_len ,128 ,realfreq_x);

toc

subplot(3,1,2)
plot(Ts(19:end),realfreq_x);  %畫出輸入波形
title('輸入波形訊號');         %標題為"輸入波形訊號"
xlabel('time(sec)');          %x軸為時間(s)
ylabel('Ampitude');           %y軸為振幅
% axis([0,10,59.9,60.1]);

% RMSE=sqrt(sum((realfreq_x-f0).^2)/942)
averlength=numel(realfreq_x);

for i=1:averlength-14                      
    averfreq(i)=(sum(realfreq_x(i:i+14)))/15;
end

% for i=1:averlength-14                        %正確版本
%     averfreq(i)=(sum(realfreq_x(i:i+14)))/15;
% end
subplot(3,1,3)
plot(Ts(33:end),averfreq)
title('輸入波形均值');         %標題為"輸入波形訊號"
xlabel('time(sec)');          %x軸為時間(s)
ylabel('Ampitude');           %y軸為振幅
