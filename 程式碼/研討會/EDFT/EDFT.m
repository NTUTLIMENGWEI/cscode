function [amp,ang,real_freq]=EDFT(length,fresult)
for i = 1:length-2;
    N=16;
    deffreq = 60;
    freq_tmp1 =  sqrt( (fresult(i)+fresult(i+2))*(fresult(i)+fresult(i+2))-4*fresult(i+1)*fresult(i+1));
    freq_sol1 = (fresult(i)+fresult(i+2)) + freq_tmp1;
    freq_sol2 = (fresult(i)+fresult(i+2)) - freq_tmp1;
    freq_tmp2(i) = freq_sol1 / (2*fresult(i+1)); 
    freq_tmp3(i) = freq_sol2 / (2*fresult(i+1)); 
    if(imag(freq_tmp2(i))>0)
        a_tmp(i) = freq_tmp2(i);
    else
        a_tmp(i) = freq_tmp3(i);
    end;
% �⦡�ҥi
    real_freq(i) = asin(imag(a_tmp(i)))*59.1234*16/(2*pi);     
    %real_freq(i) = acos(real(a_tmp(i)))*60*N/(2*pi);   
% paper
    Ar= (fresult(i+1)*a_tmp(i) - fresult(i)) / (a_tmp(i)^2 - 1);
    Ar= (fresult(i+1)*a_tmp(i) - fresult(i)) / (a_tmp(i)^2 - 1);
    fix(i) = sin(pi * (real_freq(i) - deffreq)/(60*N)) / sin(pi * (real_freq(i) - deffreq)/60);
    amp(i) = abs(Ar) * fix(i) *2 ; 
    ang(i) = (angle(Ar) - (pi / (N * 60)) * (real_freq(i) - deffreq) * (N - 1)) * 180 / pi;
end