clear all;
close all;
clc;
tic;
times_counter=0.1;

%Basic setting
time1=1/960*16*3+1/960*3;
V=1;                              %訊號電壓
F0=60.123456;                     %正弦波的頻率
N=16;                             %取樣點數               
Fs=N*60;                          %取樣頻率  
t1=0 : 1/Fs :(time1*Fs-1)/Fs;     %simpling time
Dt=1/Fs;                          %delta t = 1/fs
phi=0;                            %initial phase
% 
% time2=1/960*16*3;
% t2=0 : 1/FF*16 : (time2*(FF*16)-1)/(FF*16);       %simpling time

FF=60.11
% s1=1*cos(2*pi*F0*t1);
% s1=cos(2*pi*F0*t1);      %input signal

% s1=cos(2*pi*F0*t1)+0.05*cos(3*2*pi*F0*t1)+0.04*cos(5*2*pi*F0*t1)+0.03*cos(7*2*pi*F0*t1);      %input signal
% s1=cos(2*pi*F0*t)+0.05*cos(3*2*pi*F0*t)+0.04*cos(5*2*pi*F0*t)+0.03*cos(7*2*pi*F0*t);      %input signal

% s1=awgn(s1,60,'measured');    %加入白雜訊
% % s1=cos(2*pi*F0*t)+0.05*cos(3*2*pi*F0*t)+0.04*cos(5*2*pi*F0*t);輸入2~49階
%  for i=2:1:6
%     s1=s1+0.01*cos(2*pi*i*F0*t);
%  end

time2=1/(FF*16)*16*3;
t2=0 : 1/(FF*16) : (time2*(FF*16)-1)/(FF*16);       %simpling time

% axis([0,0.035,-inf,inf])
%reconstruction

s=1/(FF*N)-1/Fs;                    %different time of simpling

% new_s1(1)=s1(1)
for new_t=1:size(s1,2)-3                           %determine all new points

delta_xt1=s1(new_t+1)-s1(new_t);
delta_xt2=s1(new_t+2)-s1(new_t+1);
delta_xt3=s1(new_t+3)-s1(new_t+2);
delta_xxt1=delta_xt2-delta_xt1;
delta_xxt2=delta_xt3-delta_xt2;
delta_xxxt1=delta_xxt2-delta_xxt1;

new_s1(new_t+1)=s1(new_t)+(s*delta_xt1)+(0.5*s*(s-1)*delta_xxt1)+(s*(s-1)*(s-2)*delta_xxxt1)/6;
end
new_s1=new_s1(2:end);
%DFT
new_s1(1)=s1(1);         %change fist point to be an initial first point

plot(t1(1:48),s1(1:48))
xlabel('time(s)');                   
ylabel('Ampitude(u)');                  
title('Input Signal Waveform');                   
hold on
% plot(t(1:size(new_s1,2)),new_s1,'r')
plot(t2(1:48),new_s1,'r')
xlabel('time (s)');                   
ylabel('Ampitude (u)');                  
title('Input Signal Waveform');                  
% axis([0,0.05,-1.5,1.5]); 

% % save new_s2 new_s1
legend('重建前訊號','重建後訊號'); 